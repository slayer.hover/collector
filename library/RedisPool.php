<?php

class RedisPool
{
    /**
     * @var \Swoole\Coroutine\Channel
     */
    protected static $pool;

    /**
     * RedisPool constructor.
     * @param int $size 连接池的尺寸
     */
    function __construct($size = 5)
    {
		$cnf        = getConfig();
        self::$pool = new Swoole\Coroutine\Channel($cnf->swoole->task_num);
        for ($i = 0; $i < $cnf->swoole->task_num; $i++)
        {
			$res = new \Cache();
			self::$pool->push($res);
        }
    }

	public static function init()
    {
        if(!self::$pool) {
			$cnf        = getConfig();
            self::$pool = new Swoole\Coroutine\Channel($cnf->swoole->task_num);
			for ($i = 0; $i < $cnf->swoole->task_num; $i++)
			{
				$res = new \Cache();
				self::$pool->push($res);
			}
        }
        return self::$pool;
    }

    public static function put($redis)
    {
        self::init()->push($redis);
    }

    public static function get()
    {
        return self::init()->pop();
    }
}
