<?php

class MysqlPool
{
    private static $pool;      //连接池容器

    function __construct($size = 5)
    {
        $conf        = getConfig();
        $dbset    =    array(
            'dsn'        =>    'mysql:host='. $conf->mysql->host . ';dbname=' . $conf->mysql->database,
            'name'        =>    $conf->mysql->user,
            'password'    =>    $conf->mysql->password,
        );
        self::$pool = new Swoole\Coroutine\Channel($conf->swoole->task_num);
        for ($i = 0; $i < $conf->swoole->task_num; $i++)
        {
            $res = new \DB($dbset);
            self::$pool->push($res);
        }
    }

    public static function init()
    {
        if(!self::$pool) {
            $conf        = getConfig();
            $dbset    =    array(
                'dsn'        =>    'mysql:host='. $conf->mysql->host . ';dbname=' . $conf->mysql->database,
                'name'        =>    $conf->mysql->user,
                'password'    =>    $conf->mysql->password,
            );
            self::$pool = new Swoole\Coroutine\Channel($conf->swoole->task_num);
            for ($i = 0; $i < $conf->swoole->task_num; $i++)
            {
                $res = new \DB($dbset);
                self::$pool->push($res);
            }
        }
        return self::$pool;
    }

    public static function put($mysql)
    {
        self::init()->push($mysql);
    }

    public static function get()
    {
        return self::init()->pop();
    }

}
