<?php
/**
  * 图片本地化
  **/
class	ImgLocally{
	public static function writePhoto($file, $imgurl){
		if(!file_exists($file)){
            $path	=	pathinfo($file);
            if (! is_dir ( $path['dirname'] )) {  mkdir($path['dirname'], 0777, true);	}
			$wgetshell="wget -O {$file} {$imgurl}";
			shell_exec($wgetshell);
			#self::cut($file);
			return $file;
		}
		return false;
	}

	public static function writePhotoInWeb($file, $imgurl){
		if(!file_exists($file)){
            $path	=	pathinfo($file);
            if (! is_dir ( $path['dirname'] )) {  mkdir($path['dirname'], 0777, true);	}
			file_put_contents($file, file_get_contents($imgurl));
			self::cut($file);
			return $file;
		}
		return false;
	}

	#剪裁图片
	private static function cut($file){
		$im = imagecreatefromjpeg($file);
		$x	= imagesx($im);
		$y	= imagesy($im)-70;		//剪切掉底部70px
		$dim= imagecreatetruecolor($x, $y); // 创建目标图gd2
		imagecopyresized ($dim,$im,0,0,0,0,$x,$y,$x,$y);
		imagejpeg($dim, $file);
		imagedestroy($im);
		imagedestroy($dim);
	}
}
