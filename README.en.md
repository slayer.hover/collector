# 基于swoole4.0, queryList, redis连接池实现的协程队列采集器

**执行方式：CLI**
- 启动: #php launch.php start
- 停止: #php launch.php stop

**运行环境**
``` 
php >= 7.0 配置swoole4.0扩展与redis扩展
``` 

**执行顺序**
```
1. 执行php launch.php，启动采集器
2. 执行php queue.php, 生成任务队列
3. 手动将采集到的数据从Redis过滤存入数据库
```

**爬虫说明**
```
1. 执行失败的任务会进入error队列
2. 有任务失败，会自动重新检测代理池，移除失效代理。
3. 依赖库composer.json：
{
    "require": {
        "jaeger/querylist": "^4.0",
    }
}
4. App/Pickup::setRule()方法里写页面采集规则。
5. includes/config.php中的swoole->task_num定义开启的协程数量。
6. 自动开启redis连接池，池子容量与task_num协程数一致。
7. 随机代理，随机agent
8. Redis使用到的key定义：
	queue: 采集任务队列
	errorlist:任务失败队列
	proxy:代理池
```
