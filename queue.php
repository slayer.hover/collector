<?php
header('content-type:text/html;charset=utf-8');
error_reporting(E_ERROR | E_PARSE);
date_default_timezone_set('PRC');

define('ROOT_DIR', dirname(__FILE__));
define('LOG_DIR', ROOT_DIR . '/log/');
define('DEBUG_MODE', true);
define('CACHE_KEY_PREFIX', '');

require(ROOT_DIR . '/vendor/autoload.php');
require(ROOT_DIR . '/includes/autoload.php');
require(ROOT_DIR . '/includes/config.php');
require(ROOT_DIR . '/includes/function.php');
$cache = new \Cache();
$cache->select(getConfig()->redis->selectDB);
$cache->setOption(\Redis::OPT_READ_TIMEOUT, -1);

use QL\QueryList;

$baseuri = 'https://stackoverflow.com/questions';
#$ql      = QueryList::get($baseuri); var_dump($ql); exit;
$page = 4549;
do {
    echo $baseuri . '/' . $page . PHP_EOL;
    try {
        $options = [
            'max'             => 5,
            'timeout'         => 20,
            'connect_timeout' => 10,
            'delay'           => 0,
            'strict'          => false,
            'referer'         => true,
            'protocols'       => ['https'],
            'track_redirects' => false,
            'proxy'           => ['http' => 'tcp://153.35.185.71:80'],
            'headers'         => [
                'Connection'                => 'keep-alive',
                'Cache-Control'             => 'max-age=0',
                'Upgrade-Insecure-Requests' => '1',
                'Referer'                   => 'https://www.google.com/',
                'User-Agent'                => 'Mozilla/5.0 (X11; Linux x86_64; rv:29.0) Gecko/20100101 Firefox/29.0',
                'Accept'                    => 'application/json, text/plain, */*',
                'Accept-Encoding'           => 'gzip, deflate, sdch, br',
                'Cookie'                    => 'acct=t=T2eUzP8UeLJm%2fyuKQGt0UnhRl%2fGl%2fArY&s=CRPqHEqTV02J%2fAZd1evAOGPDynQI6UwH; prov=01cb55ee-8d06-f1f5-0d6e-028507545dfa; __gads=ID=9c432e6ebd6c09f5:T=1567396388:S=ALNI_MZpa6xcLdXjU36UEti3DPm3CuHt_A; __qca=P0-1756034443-1567396389201; _fbp=fb.1.1567474216582.245653868; _ga=GA1.2.181918219.1567396387; _gat=1; _gid=GA1.2.524309355.1567989827;',
            ],
        ];

        $ql   = QueryList::get($baseuri, ['tab' => 'frequent', 'pagesize' => 50, 'page' => $page], $options);
        $urls = $ql->find('div#questions .question-summary')->map(function ($item) {
            return $item->find('div.summary>h3>a')->href;
        })->all();
    } catch (\Exception $e) {
        echo 'retry' . PHP_EOL;
        usleep(rand(400000, 800000));
        continue;
    }
    #print_r($urls);   exit;
    if (is_array($urls) && !empty($urls)) {
        foreach ($urls as $k => $v) {
            if (!empty($v)) {
                $cache->lpush('queue', 'https://stackoverflow.com' . $v);
            }
        }
    } else {
        break;
    }
    $ql->destruct();
    usleep(rand(400000, 800000));
    $page++;
} while (TRUE);

echo 'Done' . PHP_EOL;


/***
 * try {
 * $client = new swoole_client(SWOOLE_SOCK_TCP);
 * $cnf        = getConfig();
 * if (!$client->connect($cnf->swoole->host, $cnf->swoole->port, -1)) {
 * throw new Exception("connect failed. Error: {$client->errCode}\n");
 * }
 * $sender = $client->send("start");
 * $n = 1;
 * while ($result = $client->recv()) {
 * echo "第{$n}次接收到内容\n";
 * if ($result == 'START') {
 * echo "任务启动\n";
 * break;
 * }
 * $n++;
 * }
 * $client->close();
 * } catch (Exception $e) {
 * echo "Exception Cached: " . $e->getMessage();
 * }
 ***/
