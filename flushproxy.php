<?php
header('content-type:text/html;charset=utf-8');
#error_reporting(E_ERROR | E_PARSE);
date_default_timezone_set('PRC');

define('ROOT_DIR', dirname(__FILE__));
define('LOG_DIR', ROOT_DIR . '/logs/');
define('DEBUG_MODE', true);
define('CACHE_KEY_PREFIX', '');

require(ROOT_DIR . '/vendor/autoload.php');
require(ROOT_DIR . '/includes/autoload.php');
require(ROOT_DIR . '/includes/config.php');
require(ROOT_DIR . '/includes/function.php');

use QL\QueryList;

final class Pickup
{
    private static $ql;
    private $cache;
    private $db;

    public function __construct($cache)
    {
        if (self::$ql == null) self::$ql = QueryList::getInstance();
        $this->cache = $cache;
    }

    public function getProxy()
    {
        $this->getProxies1();
        $this->getProxies2();
        $this->getProxies3();
        $this->getProxies4();
    }

    public function checkProxy($proxy)
    {
        if (!@stream_socket_client($proxy, $errno, $errstr, 1)) {
            \Log::out('launch', "[remove proxy]:" . $proxy);
            $this->cache->srem('proxy', $proxy);
        }
    }

    public function checkAllProxy(): bool
    {
        foreach ($this->cache->smembers('proxy') as $v) {
            $info = parse_url($v);
            if (!@stream_socket_client($info['host'] . ':' . $info['port'], $errno, $errstr, 1)) {
                $this->cache->srem('proxy', $v);
            }
        }
        return true;
    }

    public function getProxies1()
    {
        $baseuri = 'http://www.iphai.com/free/ng';
        $proxy   = self::$ql->get($baseuri)->find('.table.table-striped tr')->map(function ($tr) {
            return $tr->find('td')->texts();
        })->all();
        for ($i = 1; $i <= 30; $i++) {
            \Log::out('launch', "[check proxies]:" . $proxy[$i][0]);
            if (!empty(trim($proxy[$i][0])) && @stream_socket_client(trim($proxy[$i][0]) . ":" . trim($proxy[$i][1]), $errno, $errstr, 1)) {
                $scheme = empty($proxy[$i][3]) ? 'http' : strtolower($proxy[$i][3]);
                $this->cache->sadd('proxy', $scheme . '://' . trim($proxy[$i][0]) . ':' . trim($proxy[$i][1]));
            }
        }
    }
    public function getProxies2()
    {
        $baseuri = 'http://www.iphai.com/free/wg';
        $proxy   = self::$ql->get($baseuri)->find('.table.table-striped tr')->map(function ($tr) {
            return $tr->find('td')->texts();
        })->all();
        for ($i = 1; $i <= 30; $i++) {
            \Log::out('launch', "[check proxies]:" . $proxy[$i][0]);
            if (!empty(trim($proxy[$i][0])) && @stream_socket_client(trim($proxy[$i][0]) . ":" . trim($proxy[$i][1]), $errno, $errstr, 1)) {
                $scheme = empty($proxy[$i][3]) ? 'http' : strtolower($proxy[$i][3]);
                $this->cache->sadd('proxy', $scheme . '://' . trim($proxy[$i][0]) . ':' . trim($proxy[$i][1]));
            }
        }
    }
    public function getProxies3()
    {
        $baseuri = 'http://www.iphai.com/free/wp';
        $proxy   = self::$ql->get($baseuri)->find('.table.table-striped tr')->map(function ($tr) {
            return $tr->find('td')->texts();
        })->all();
        for ($i = 1; $i <= 30; $i++) {
            \Log::out('launch', "[check proxies]:" . $proxy[$i][0]);
            if (!empty(trim($proxy[$i][0])) && @stream_socket_client(trim($proxy[$i][0]) . ":" . trim($proxy[$i][1]), $errno, $errstr, 1)) {
                $scheme = empty($proxy[$i][3]) ? 'http' : strtolower($proxy[$i][3]);
                $this->cache->sadd('proxy', $scheme . '://' . trim($proxy[$i][0]) . ':' . trim($proxy[$i][1]));
            }
        }
    }

    public function getProxies4()
    {
        $baseuri = 'https://www.xicidaili.com/nt/';
        $proxy   = self::$ql->get($baseuri)->find('table#ip_list tr')->map(function ($tr) {
            return $tr->find('td')->texts();
        })->all();
        for ($i = 1; $i <= 100; $i++) {
            \Log::out('launch', "[check proxies]:" . $proxy[$i][1]);
            if (!empty(trim($proxy[$i][1])) && @stream_socket_client(trim($proxy[$i][1]) . ":" . trim($proxy[$i][2]), $errno, $errstr, 1)) {
                $this->cache->sadd('proxy', strtolower($proxy[$i][5]) . '://' . trim($proxy[$i][1]) . ':' . trim($proxy[$i][2]));
            }
        }
    }

    public function randUserAgent($type = 'pc')
    {
        switch ($type) {
            case 'pc':
                return $this->userAgentArray['pc'][array_rand($this->userAgentArray['pc'])] . rand(0, 10000);
                break;
            case 'android':
                return $this->userAgentArray['android'][array_rand($this->userAgentArray['android'])] . rand(0, 10000);
                break;
            case 'ios':
                return $this->userAgentArray['ios'][array_rand($this->userAgentArray['ios'])] . rand(0, 10000);
                break;
            case 'mobile':
                $userAgentArray = array_merge($this->userAgentArray['android'], $this->userAgentArray['ios']);
                return $userAgentArray[array_rand($userAgentArray)] . rand(0, 10000);
            default:
                return $type;
                break;
        }
    }

    public $userAgentArray = [
        'pc'      => [
            'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36',
            'Mozilla/5.0 (X11; Linux x86_64; rv:29.0) Gecko/20100101 Firefox/29.0',
            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:29.0) Gecko/20100101 Firefox/29.0',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/537.75.14',
            'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:29.0) Gecko/20100101 Firefox/29.0',
            'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36',
            'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko',
        ],
        'android' => [
            'Mozilla/5.0 (Android; Mobile; rv:29.0) Gecko/29.0 Firefox/29.0',
            'Mozilla/5.0 (Linux; Android 4.4.2; Nexus 4 Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.114 Mobile Safari/537.36',
        ],
        'ios'     => [
            'Mozilla/5.0 (iPad; CPU OS 7_0_4 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) CriOS/34.0.1847.18 Mobile/11B554a Safari/9537.53',
            'Mozilla/5.0 (iPad; CPU OS 7_0_4 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11B554a Safari/9537.53',
            'Mozilla/5.0 (iPhone; CPU iPhone OS 8_0_2 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12A366 Safari/600.1.4',
            'Mozilla/5.0 (iPhone; CPU iPhone OS 8_0 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12A366 Safari/600.1.4',
        ],
    ];
}

go(function () {
    $starton = microtime(true);
    $cache   = \RedisPool::get();
    (new Pickup($cache))->getProxy();
    $time = round(microtime(true) - (float)$starton, 5);
    echo '浪费计算时间共：', $time, '    浪费内存共计：', (memory_get_usage(true) / 1024), "kb\n\nDone.\n";
});
