<?php
header('content-type:text/html;charset=utf-8');
error_reporting(E_ERROR | E_PARSE);
date_default_timezone_set('PRC');
ini_set('memory_limit', -1);

define('ROOT_DIR', dirname(__FILE__).'/..');
define('LOG_DIR', ROOT_DIR . '/log/');
define('DEBUG_MODE', true);
define('CACHE_KEY_PREFIX', '');

require(ROOT_DIR . '/vendor/autoload.php');
require(ROOT_DIR . '/includes/autoload.php');
require(ROOT_DIR . '/includes/config.php');
require(ROOT_DIR . '/includes/function.php');

function getData($_DB, $start_id, $end_id)
{
    $sql  = "select * from qa_question where id>={$start_id} and id<={$end_id} and question is null order by id asc";
    $stat = $_DB->pdo->query($sql);
    while ($row = $stat->fetch()) {
        yield $row;
    }
}

$conf  = getConfig();
$dbset = [
    'dsn'      => 'mysql:host=' . $conf->mysql->host . ';dbname=' . $conf->mysql->database,
    'name'     => $conf->mysql->user,
    'password' => $conf->mysql->password,
];

if ($argc < 4) {
    echo 'argument number error!' . PHP_EOL;
    echo 'arg 1:trans conf order key (a,b,c,d,e,f,g,h,i,j);' . PHP_EOL . 'arg 2:question table start id;' . PHP_EOL . 'arg 3:question table end id;' . PHP_EOL;
    exit;
}
$transConf = $argv[1];
$start_id  = $argv[2];
$end_id    = $argv[3];
if ($end_id < $start_id) {
    echo 'argument end_id less than start_id' . PHP_EOL;
    exit;
}
$_DB  = new \DB($dbset);
$tran = new \Trans($conf->trans->$transConf);

foreach (getData($_DB, $start_id, $end_id) as $row) {
    if (empty($row['title_raw'])) continue;
    #翻译问题正文
    if (preg_match_all('#(<pre>)?<code>.*?</code>(</pre>)?#isu', $row['question_raw'], $code) > 0) {
        $ocrstr   = preg_replace('#(<pre>)?<code>.*?</code>(</pre>)?#isu', '{c.o.d.e}', $row['question_raw']);
        $transer  = getTrans($tran->run($ocrstr), function () use ($tran, $ocrstr) {
            return $tran->run($ocrstr);
        });
        $transer  = str_replace('{C.O.D.E}', '{c.o.d.e}', $transer);
        $deal     = explode('{c.o.d.e}', $transer);
        $question = '';
        foreach ($deal as $k2 => $v2) {
            $question .= $v2;
            if (isset($code[0][$k2])) {
                $question .= $code[0][$k2];
            }
        }
    } else {
        $ocrstr   = $row['question_raw'];
        $question = getTrans($tran->run($ocrstr), function () use ($tran, $ocrstr) {
            return $tran->run($ocrstr);
        });
    }
    #翻译问题评论
    $comment = $row['comment_raw'];
    if (!empty($comment)) {
        preg_match_all('#<div\s+class\=\"comment\-body\s+js\-comment\-edit\-hide\">\s+(.*?)\s+&ndash;&nbsp;<a\s+href\=\"#isu', $comment, $trans);
        foreach ($trans[1] as $k1 => $v1) {
            $transer = '';
            if (preg_match_all('#(<pre>)?<code>.*?</code>(</pre>)?#isu', $v1, $code) > 0) {
                $ocrstr  = preg_replace('#(<pre>)?<code>.*?</code>(</pre>)?#isu', '{c.o.d.e}', $v1);
                $transer = getTrans($tran->run($ocrstr), function () use ($tran, $ocrstr) {
                    return $tran->run($ocrstr);
                });
                $transer = str_replace('{C.O.D.E}', '{c.o.d.e}', $transer);
                $deal    = explode('{c.o.d.e}', $transer);
                $transer = '';
                foreach ($deal as $k2 => $v2) {
                    $transer .= $v2;
                    if (isset($code[0][$k2])) {
                        $transer .= $code[0][$k2];
                    }
                }
            } else {
                $ocrstr  = $v1;
                $transer = getTrans($tran->run($ocrstr), function () use ($tran, $ocrstr) {
                    return $tran->run($ocrstr);
                });
            }
            $comment = str_replace($v1, $transer, $comment);
        }
    }
    $ocrstr = $row['title_raw'];
    $title  = getTrans($tran->run($ocrstr), function () use ($tran, $ocrstr) {
        return $tran->run($ocrstr);
    });
    #更新数据
    $questionRow = [
        'title'      => $title,
        'question'   => $question,
        'comment'    => $comment,
    ];
    $_DB->update("qa_question", $questionRow, "id='{$row['id']}'");
    echo $row['id'] . PHP_EOL;
}

echo 'Done' . PHP_EOL;
