<?php
header('content-type:text/html;charset=utf-8');
error_reporting(E_ERROR | E_PARSE);
date_default_timezone_set('PRC');
ini_set('memory_limit', -1);

define('ROOT_DIR', dirname(__FILE__) . '/..');
define('LOG_DIR', ROOT_DIR . '/log/');
define('DEBUG_MODE', true);
define('CACHE_KEY_PREFIX', '');

require(ROOT_DIR . '/vendor/autoload.php');
require(ROOT_DIR . '/includes/autoload.php');
require(ROOT_DIR . '/includes/config.php');
require(ROOT_DIR . '/includes/function.php');

function getData($_DB, $start_id, $end_id)
{
    #max run to 89487
    $sql  = "select * from qa_answer where id>={$start_id} and id<={$end_id} order by id asc";
    $stat = $_DB->pdo->query($sql);
    while ($row = $stat->fetch()) {
        yield $row;
    }
}

$conf  = getConfig();
$dbset = [
    'dsn'      => 'mysql:host=' . $conf->mysql->host . ';dbname=' . $conf->mysql->database,
    'name'     => $conf->mysql->user,
    'password' => $conf->mysql->password,
];

if ($argc < 4) {
    echo 'argument number error!' . PHP_EOL;
    echo 'arg 1:trans conf order key (a,b,c,d,e,f,g,h,i,j);' . PHP_EOL . 'arg 2:answer table start id;' . PHP_EOL . 'arg 3:answer table end id;' . PHP_EOL;
    exit;
}
$transConf = $argv[1];
$start_id  = $argv[2];
$end_id    = $argv[3];
if ($end_id < $start_id) {
    echo 'argument end_id less than start_id' . PHP_EOL;
    exit;
}
$_DB  = new \DB($dbset);
$tran = new \Trans($conf->trans->$transConf);

foreach (getData($_DB, $start_id, $end_id) as $row) {
    if (empty($row['answer_raw'])) continue;
    #翻译回答正文
    if (preg_match_all('#(<pre>)?<code>.*?</code>(</pre>)?#isu', $row['answer_raw'], $code) > 0) {
        $ocrstr  = preg_replace('#(<pre>)?<code>.*?</code>(</pre>)?#isu', '{c.o.d.e}', $row['answer_raw']);
        $transer = getTrans($tran->run($ocrstr), function () use ($tran, $ocrstr) {
            return $tran->run($ocrstr);
        });
        $transer = str_replace('{C.O.D.E}', '{c.o.d.e}', $transer);
        $deal    = explode('{c.o.d.e}', $transer);
        $answer  = '';
        foreach ($deal as $k2 => $v2) {
            $answer .= $v2;
            if (isset($code[0][$k2])) {
                $answer .= $code[0][$k2];
            }
        }
    } else {
        $ocrstr = $row['answer_raw'];
        $answer = getTrans($tran->run($ocrstr), function () use ($tran, $ocrstr) {
            return $tran->run($ocrstr);
        });
    }
    #翻译回答评论
    $comment = $row['comment_raw'];
    if (!empty($comment)) {
        preg_match_all('#<div\s+class\=\"comment\-body\s+js\-comment\-edit\-hide\">\s+(.*?)\s+&ndash;&nbsp;<a\s+href\=\"#isu', $comment, $trans);
        foreach ($trans[1] as $k1 => $v1) {
            $transer = '';
            if (preg_match_all('#(<pre>)?<code>.*?</code>(</pre>)?#isu', $v1, $code) > 0) {
                $ocrstr  = preg_replace('#(<pre>)?<code>.*?</code>(</pre>)?#isu', '{c.o.d.e}', $v1);
                $transer = getTrans($tran->run($ocrstr), function () use ($tran, $ocrstr) {
                    return $tran->run($ocrstr);
                });
                $transer = str_replace('{C.O.D.E}', '{c.o.d.e}', $transer);
                $deal    = explode('{c.o.d.e}', $transer);
                $transer = '';
                foreach ($deal as $k2 => $v2) {
                    $transer .= $v2;
                    if (isset($code[0][$k2])) {
                        $transer .= $code[0][$k2];
                    }
                }
            } else {
                $ocrstr  = $v1;
                $transer = getTrans($tran->run($ocrstr), function () use ($tran, $ocrstr) {
                    return $tran->run($ocrstr);
                });
            }
            $comment = str_replace($v1, $transer, $comment);
        }
    }
    #更新数据
    $answerRow = [
        'answer'     => $answer,
        'comment'    => $comment,
        'updated_at' => date('Y-m-d H:i:s'),
    ];
    $_DB->update("qa_answer", $answerRow, "id='{$row['id']}'");
    echo $row['id'] . PHP_EOL;
}

echo 'Trans '. $transConf  . ' Done!' . PHP_EOL;
