<?php
header('content-type:text/html;charset=utf-8');
error_reporting(E_ERROR | E_PARSE);
date_default_timezone_set('PRC');
ini_set('memory_limit', -1);

define('ROOT_DIR', dirname(__FILE__) . '/..');
define('LOG_DIR', ROOT_DIR . '/log/');
define('DEBUG_MODE', true);
define('CACHE_KEY_PREFIX', '');

require(ROOT_DIR . '/vendor/autoload.php');
require(ROOT_DIR . '/includes/autoload.php');
require(ROOT_DIR . '/includes/config.php');
require(ROOT_DIR . '/includes/function.php');

function getData($_DB, $start_id, $end_id)
{
    $sql  = "select * from qa_question where id>={$start_id} and id<={$end_id} order by id asc";
    $stat = $_DB->pdo->query($sql);
    while ($row = $stat->fetch()) {
        yield $row;
    }
}

$conf  = getConfig();
$dbset = [
    'dsn'      => 'mysql:host=' . $conf->mysql->host . ';dbname=' . $conf->mysql->database,
    'name'     => $conf->mysql->user,
    'password' => $conf->mysql->password,
];
if ($argc < 3) {
    echo 'argument number error!' . PHP_EOL;
    echo 'arg 1:question table start id;' . PHP_EOL . 'arg 2:question table end id;' . PHP_EOL;
    exit;
}
$start_id = $argv[1];
$end_id   = $argv[2];
if ($end_id < $start_id) {
    echo 'argument end_id less than start_id' . PHP_EOL;
    exit;
}
$_DB = new \DB($dbset);

$targetDatabase = 'qalists';
$target_dbset   = [
    'dsn'      => 'mysql:host=' . $conf->mysql->host . ';dbname=' . $targetDatabase,
    'name'     => $conf->mysql->user,
    'password' => $conf->mysql->password,
];
$_targetDB      = new \DB($target_dbset);

#转移数据
try {
    foreach (getData($_DB, $start_id, $end_id) as $key => $row) {
        $num          = $key % 10 + 1;
        $targetTable  = 'qa_question0' . ($num < 10 ? '0' : '') . $num;
        $question_id  = 'Q0' . ($num < 10 ? '0' : '') . $num . substr(md5(md5($row['id'])), 4);
        $$targetTable = [
            'question_id' => $question_id,
            'sof_id'      => $row['sfid'],
            'title'       => $row['title'],
            'title_raw'   => $row['title_raw'],
            'tags'        => $row['tags'],
            'user'        => $row['user'],
            'created_at'  => $row['created_at'],
        ];
        $_targetDB->add($targetTable, $$targetTable);
        $targetConent  = $targetTable . '_content';
        $$targetConent = [
            'question_id'  => $question_id,
            'question'     => $row['question'],
            'question_raw' => $row['question_raw'],
        ];
        $_targetDB->add($targetConent, $$targetConent);
        if (!empty(trim($row['comment']))) {
            $targetComment  = $targetTable . '_comment';
            $$targetComment = [
                'question_id' => $question_id,
                'comment'     => $row['comment'],
                'comment_raw' => $row['comment_raw'],
            ];
            $_targetDB->add($targetComment, $$targetComment);
        }
        #问题回答
        $sql                 = "select * from qa_answer where question_id={$row['id']}";
        $answer              = $_DB->getAll($sql);
        $targetAnswer        = $targetTable . '_answer';
        $targetAnswerComment = $targetTable . '_answer_comment';
        if (is_array($answer) && !empty($answer)) {
            foreach ($answer as $k1 => $v1) {
                $$targetAnswer = [
                    'question_id' => $question_id,
                    'answer'      => $v1['answer'],
                    'answer_raw'  => $v1['answer_raw'],
                    'user'        => $v1['user'],
                    'flag'        => $v1['flag'],
                    'created_at'  => $v1['created_at'],
                ];
                $answer_id     = $_targetDB->add($targetAnswer, $$targetAnswer);
                if (!empty(trim($v1['comment']))) {
                    $$targetAnswerComment = [
                        'answer_id'   => $answer_id,
                        'comment'     => $v1['comment'],
                        'comment_raw' => $v1['comment_raw'],
                    ];
                    $_targetDB->add($targetAnswerComment, $$targetAnswerComment);
                }
            }
        }
        echo $row['id'] . PHP_EOL;
    }
} catch (Exception $e) {
    echo $e->getMessage();
}

echo 'Transfer ' . $start_id . ' - ' . $end_id . ' Done!' . PHP_EOL;
